@echo off 
rem Please, modify and run the 'wsetenv.bat' batch file first in order to
rem setup path to built binaries, etc.

echo Filtering input image...
mdsLoadDicom <../data/dicom/80.dcm |mdsSliceFilter -filter anisotropic >sample4.slc
mdsSliceRange -auto <sample4.slc |mdsSliceView

echo Watershed transform...
mdsSliceSegWatersheds -wsheds <sample4.slc |mdsSliceRange -auto |mdsSliceView

echo Watershed segmentation...
mdsSliceSegWatersheds -merge -simthr 0.1 <sample4.slc |mdsSliceView -coloring segmented

