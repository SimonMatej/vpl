//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2009 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2007/06/21                       
 *
 * Description:
 * - Binary serialization of objects (data entities) over shared memory.
 */

#include <VPL/Module/SHMSerializer.h>


namespace vpl
{
namespace mod
{

//==============================================================================
/*
 * Global definitions and functions.
 */



} // namespace mod
} // namespace vpl

