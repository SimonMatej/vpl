#==============================================================================
# This file comes from MDSTk software and was modified for
#
# VPL - Voxel Processing Library
# Changes are Copyright 2014 3Dim Laboratory s.r.o.
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# The original MDSTk legal notice can be found below.
# 
# Medical Data Segmentation Toolkit (MDSTk)
# Copyright (c) 2007 by PGMed@FIT
#
# Authors: Miroslav Svub, svub@fit.vutbr.cz
#          Michal Spanel, spanel@fit.vutbr.cz
# Date:    2007/08/25
#==============================================================================

SET( VPL_TESTS ConsoleTEST )

FOREACH( VPL_TEST ${VPL_TESTS} )

  ADD_EXECUTABLE( ${VPL_TEST} ${VPL_TEST}.cpp )
  TARGET_LINK_LIBRARIES( ${VPL_TEST}
                         ${VPL_LIBS} 
                         ${VPL_SYSTEM_LIBS} 
                         ${VPL_ZLIB} )
  SET_TARGET_PROPERTIES( ${VPL_TEST} PROPERTIES
                         DEBUG_POSTFIX d
                         LINK_FLAGS "${VPL_LINK_FLAGS}" )
   INSTALL( TARGETS ${VPL_TEST}
    RUNTIME DESTINATION test/vpl/Module )

ENDFOREACH( VPL_TEST )

