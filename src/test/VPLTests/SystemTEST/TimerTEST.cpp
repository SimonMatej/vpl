//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/11/01                       
 *
 * Description:
 * - Testing of the vpl::CTimer class.
 */

#include <VPL/System/ScopedLock.h>
#include <VPL/System/Thread.h>
#include <VPL/System/Sleep.h>
#include <VPL/System/Timer.h>

// STL
#include <iostream>
#include <cstdlib>

//==============================================================================
/*
 * Global constants.
 */

//! The number of threads and timers
#define THREADS     3

//! Delay after releasing mutex
#define DELAY       500

//! Mutex
vpl::sys::CMutexPtr spMutex;

//! Timers
vpl::sys::CTimer    *ppTimers[THREADS];


//==============================================================================
/*!
 * Generates random number
 */
unsigned random(const unsigned uiMax)
{
    return (1 + (unsigned int)(((double)rand() / RAND_MAX) * uiMax));
}


//==============================================================================
/*!
 * Thread routine
 */
VPL_THREAD_ROUTINE(thread)
{
    int id = *(reinterpret_cast<int *>(pThread->getData()));

    VPL_THREAD_MAIN_LOOP
    {
        if( ppTimers[id]->wait(random(DELAY)) )
        {
            vpl::sys::tScopedLock Guard1(*spMutex);
            std::cout << "Thread: " << id << std::endl;
        }
    }

    vpl::sys::tScopedLock Guard2(*spMutex);
    std::cout << "Thread: " << id << " terminated" << std::endl;

    return 0;
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    // Creation of all timers
    for( int i = 0; i < THREADS; i++ )
    {
        ppTimers[i] = new vpl::sys::CTimer(DELAY + DELAY * i, true);
    }

    // Threads
    vpl::sys::CThread *ppThreads[THREADS];
    int piThreadsId[THREADS];

    // Creation of all ppThreads
    for( int i = 0; i < THREADS; i++ )
    {
        piThreadsId[i] = i;
        ppThreads[i] = new vpl::sys::CThread(thread, (void *)&piThreadsId[i], true);
    }

    // Sleep
    vpl::sys::sleep(10000);

    // Destroy all ppThreads
    for( int i = 0; i < THREADS; i++ )
    {
        ppThreads[i]->terminate(true, 1000);
        delete ppThreads[i];
    }

    // Destroy all timers
    for( int i = 0; i < THREADS; i++ )
    {
        ppTimers[i]->stop();
        delete ppTimers[i];
    }

    return 0;
}

