#################################################################################
# This file is part of
#
# VPL - Voxel Processing Library
# Copyright 2014 3Dim Laboratory s.r.o.
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#################################################################################

set(TEST_NAME SystemTest)

include_directories({VPL_SOURCE_DIR}/include/)


VPL_TEST_INCLUDE_DIR(${CMAKE_CURRENT_SOURCE_DIR})

if(VPL_GTESTS_PREBUILTS)
    include_directories(${GTEST_INCLUDE_DIRS})
endif()


ADD_SOURCE_DIRECTORY( ${CMAKE_CURRENT_SOURCE_DIR} )
add_executable(${TEST_NAME} ${VPL_SOURCES} ${VPL_HEADERS})

target_link_libraries( ${TEST_NAME} ${VPL_LIBS} )
set_target_properties( ${TEST_NAME} PROPERTIES
                         DEBUG_POSTFIX d
                         LINK_FLAGS "${VPL_LINK_FLAGS}" )
 
 
if(VPL_GTESTS_PREBUILTS)
    target_link_libraries(${TEST_NAME}  ${GTEST_BOTH_LIBRARIES})
else()
    
    add_dependencies( ${TEST_NAME}  gtest )
    link_directories(${GTEST_DIRECTORY_LIBS})
    target_link_libraries( ${TEST_NAME} 
        debug ${GTEST_DEBUG_LIBRARIES}
        optimized ${GTEST_RELEASE_LIBRARIES}
 )
endif()



INSTALL( TARGETS ${TEST_NAME}
        RUNTIME DESTINATION test/gtest )

add_test(AllTestsInSystem ${TEST_NAME})


    

