//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"

#include <VPL/System/Sleep.h>
#include <VPL/System/Thread.h>
#include "VPL/System/NamedSemaphore.h"

namespace mutex
{


//==============================================================================
//! Global constants.

namespace settings
{
const int numberOfThreads = 5;
const int delay = 50;

//! Name of the sempahore
const std::string semaphoreName = "1234";
}

//! Smart pointer to mutex
vpl::sys::CMutexPtr     spMutex;

//! Buffer for test checks
bool buffer[settings::numberOfThreads] = { false };

//==============================================================================
//! Generates random number
unsigned random(const unsigned uiMax)
{
    return (1 + static_cast<unsigned int>((static_cast<double>(rand()) / RAND_MAX) * uiMax));
}

//==============================================================================
//! Thread routine
VPL_THREAD_ROUTINE(thread)
{
	const int id = *(reinterpret_cast<int *>(pThread->getData()));

    VPL_THREAD_MAIN_LOOP
    {
        spMutex->lock();
    vpl::sys::sleep(random(settings::delay));
    buffer[id] = true;
    vpl::sys::sleep(random(settings::delay));
    spMutex->unlock();
    vpl::sys::sleep(random(settings::delay));
    }

    spMutex->lock();
    spMutex->unlock();

    return 0;
}

//! Create threads and checks valid values in buffer.
TEST(MutexTest, Base)
{
    // Threads
    vpl::sys::CThread *ppThreads[settings::numberOfThreads];
    int piThreadsId[settings::numberOfThreads];

    // Creation of all ppThreads
    for (int i = 0; i < settings::numberOfThreads; i++)
    {
        piThreadsId[i] = i;
        ppThreads[i] = new vpl::sys::CThread(thread, static_cast<void *>(&piThreadsId[i]), true);
    }

    // Sleep
    vpl::sys::sleep(1000);

    // Destroy all ppThreads
    for (int i = 0; i < settings::numberOfThreads; i++)
    {
        ppThreads[i]->terminate(true, 1000);
        delete ppThreads[i];
    }

    // Check test buffer, if all threads write to them.
    for (int i = 0; i < settings::numberOfThreads; i++)
    {
        ASSERT_TRUE(buffer[i]);
    }
}

}

