//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"
#include <VPL/Base/SharedPtr.h>
#include "VPL/Test/Utillities/requiredData.h"

namespace shared
{
class CA : public vpl::base::CObject
{
public:
    VPL_SHAREDPTR(CA);

    //! Internal data
    int m_iData;

    //! Constructor
    CA() : m_iData(0) {}

    //! Constructor
    CA(int iData) : m_iData(iData) {}

    //! Copy constructor
    CA(const CA& a) : vpl::base::CObject(), m_iData(a.m_iData) {}

    virtual ~CA() {}

    virtual void check(vpl::test::RequiredData required)
    {
        if (required.count() < 3)
        {
            FAIL() << "not all arguments set.";
        }
		ASSERT_EQ(required.getNumber<int>("dataCA"), m_iData);
		ASSERT_EQ(required.getNumber<int>("referenceCount"), getReferencesCount());
		ASSERT_EQ(required.getBool("isHeap"), isOnHeap());
	}
};

//! Smart pointer to the class CA
typedef CA::tSmartPtr CAPtr;




class CAA : public CA
{
public:
    VPL_SHAREDPTR(CAA);

    //! Internal data
    int m_iData2;
    CAA() : m_iData2(0) {}

    //! Constructor
    CAA(int iData, int iData2) : CA(iData), m_iData2(iData2) {}

    //! Copy constructor
    CAA(const CAA& a) : CA(a), m_iData2(a.m_iData2) {}

    virtual ~CAA() {}

    virtual void check(vpl::test::RequiredData& required)
    {
        if(required.count()< 4)
        {
            FAIL() << "not all arguments set.";
        }
        ASSERT_EQ(required.getNumber<int>("dataCA"), m_iData);
        ASSERT_EQ(required.getNumber<int>("dataCAA"), m_iData2);
        ASSERT_EQ(required.getNumber<int>("referenceCount"), getReferencesCount());
        ASSERT_EQ(required.getBool("isHeap"),isOnHeap());
    }
};

//! Smart pointer to the class CAA
typedef CAA::tSmartPtr CAAPtr;

class SharedPtrTest : public testing::Test
{
public:
    CAPtr p1;
    CAPtr p2;


    CAAPtr pp1;
    CAAPtr pp2;
    void SetUp() override
    {

    }
    void TearDown() override
    {

    }
};

   

TEST_F(SharedPtrTest, ConstructorDefault)
{
	vpl::test::RequiredData data;
	data.require("dataCA", 0);
	data.require("referenceCount", 1);
	data.require("isHeap", true);

    CAPtr p1;
    p1->check(data);
}

TEST_F(SharedPtrTest, ConstructorArg)
{
    p2 = CAPtr(new CA(100));
	vpl::test::RequiredData data;
	data.require("dataCA", 100);
	data.require("referenceCount", 1);
	data.require("isHeap", true);

    p2->check(data);
}

TEST_F(SharedPtrTest, ConstructorCopy)
{
	vpl::test::RequiredData data;
	data.require("dataCA", 0);
	data.require("referenceCount", 1);
	data.require("isHeap", true);

    p2 = CAPtr(new CA(*p1));
    p2->check(data);

	data.require("dataCA", 1);
	data.require("referenceCount", 1);
	data.require("isHeap", true);

    p2->m_iData += 1;
    p2->check(data);

	data.require("dataCA", 0);
	data.require("referenceCount", 1);
	data.require("isHeap", true);

    p1->check(data);

}

TEST_F(SharedPtrTest, ConstructorCRefCountPtr)
{
	vpl::test::RequiredData data;
	data.require("dataCA", 0);
	data.require("referenceCount", 1);
	data.require("isHeap", true);

    p1->check(data);

    {
        CAPtr p2(p1);

		data.require("dataCA", 0);
		data.require("referenceCount", 2);
		data.require("isHeap", true);
        p2->check(data);

        p2->m_iData += 1;

		data.require("dataCA", 1);
		data.require("referenceCount", 2);
		data.require("isHeap", true);
        p2->check(data);

		data.require("dataCA", 1);
		data.require("referenceCount", 2);
		data.require("isHeap", true);
        p1->check(data);
    }
	data.require("dataCA", 1);
	data.require("referenceCount", 1);
	data.require("isHeap", true);
    p1->check(data);

}

TEST_F(SharedPtrTest, ConstructorDefaultChild)
{
	vpl::test::RequiredData data;
	data.require("dataCA", 0);
	data.require("dataCAA", 0);
	data.require("referenceCount", 1);
	data.require("isHeap", true);

    pp1->check(data);
}


TEST_F(SharedPtrTest, ConstructorArgChild)
{
    pp2 = CAAPtr(new CAA(1, 2));
	vpl::test::RequiredData data;
	data.require("dataCA", 1);
	data.require("dataCAA", 2);
	data.require("referenceCount", 1);
	data.require("isHeap", true);

    pp2->check(data);

	data.require("dataCA", 0);
	data.require("dataCAA", 0);
	data.require("referenceCount", 1);
	data.require("isHeap", true);
    pp1->check(data);
}

TEST_F(SharedPtrTest, ConstructorCopyChild)
{
    pp2 = CAAPtr(new CAA(*pp1));

	vpl::test::RequiredData data;
	data.require("dataCA", 0);
	data.require("dataCAA", 0);
	data.require("referenceCount", 1);
	data.require("isHeap", true);

    pp2->check(data);

    pp2->m_iData2 += 1;
    pp2->m_iData += 1;

	data.require("dataCA", 1);
	data.require("dataCAA", 1);
	data.require("referenceCount", 1);
	data.require("isHeap", true);
    pp2->check(data);

	data.require("dataCA", 0);
	data.require("dataCAA", 0);
	data.require("referenceCount", 1);
	data.require("isHeap", true);

    pp1->check(data);
}

TEST_F(SharedPtrTest, ConstructorCRefCountPtrChild)
{
    {
        CAAPtr pp2 = CAAPtr(pp1);

		vpl::test::RequiredData data;
		data.require("dataCA", 0);
		data.require("dataCAA", 0);
		data.require("referenceCount", 2);
		data.require("isHeap", true);

        pp2->check(data);

        pp2->m_iData += 1;
        pp2->m_iData2 += 1;

		data.require("dataCA", 1);
		data.require("dataCAA", 1);
		data.require("referenceCount", 2);
		data.require("isHeap", true);
        pp2->check(data);

        ASSERT_EQ(2, pp1->getReferencesCount());

    }

	vpl::test::RequiredData data;
	data.require("dataCA", 1);
	data.require("dataCAA", 1);
	data.require("referenceCount", 1);
	data.require("isHeap", true);
    pp1->check(data);

    {
        CAPtr p2(static_cast<CA *>(pp1));

		data.require("dataCA", 1);
		data.require("dataCAA", 1);
		data.require("referenceCount", 2);
		data.require("isHeap", true);
        pp1->check(data);

        p2->m_iData += 1;
		data.require("dataCA", 2);
		data.require("dataCAA", 1);
		data.require("referenceCount", 2);
		data.require("isHeap", true);
        pp1->check(data);
    }
	data.require("dataCA", 2);
	data.require("dataCAA", 1);
	data.require("referenceCount", 1);
	data.require("isHeap", true);
    pp1->check(data);
}

TEST_F(SharedPtrTest, AssignPtr)
{
    CAAPtr pp1 = CAAPtr(new CAA(2, 1));
    {
        CAPtr p2(new CA(1));

		vpl::test::RequiredData data;
		data.require("dataCA", 1);
		data.require("referenceCount", 1);
		data.require("isHeap", true);
        p2->check(data);

        p2 = static_cast<CA *>(pp1);
		data.require("dataCA", 2);
		data.require("dataCAA", 1);
		data.require("referenceCount", 2);
		data.require("isHeap", true);
        p2->check(data);
    }
	vpl::test::RequiredData data;
	data.require("dataCA", 2);
	data.require("dataCAA", 1);
	data.require("referenceCount", 1);
	data.require("isHeap", true);
    pp1->check(data);
}
TEST_F(SharedPtrTest, AssignObject)
{
	vpl::test::RequiredData data;
	data.require("dataCA",0);
	data.require("referenceCount", 1);
	data.require("isHeap", false);
    CA a1;
    a1.check(data);
    {
        CAPtr p2(&a1);

		data.require("dataCA", 0);
		data.require("referenceCount", 2);
		data.require("isHeap", false);

        p2->check(data);
        p2->m_iData += 1;
		data.require("dataCA", 1);
		data.require("referenceCount", 2);
		data.require("isHeap", false);
        p2->check(data);
    }
	data.require("dataCA", 1);
	data.require("referenceCount", 1);
	data.require("isHeap", false);
    a1.check(data);
}
}
