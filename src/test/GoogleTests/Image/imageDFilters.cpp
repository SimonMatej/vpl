//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/


#include "gtest/gtest.h"
#include "VPL/Image/Image.h"
#include <VPL/Test/Utillities/imageMetrics.h>
#include "VPL/Module/DensityCompressor.h"
#include <VPL/Image/ImageFunctions.h>
#include <VPL/Test/Utillities/arguments.h>
#include "VPL/Image/Filters/Gaussian.h"
#include "VPL/ImageIO/DicomSliceLoaderVPL.h"
#include "VPL/ImageIO/DicomSliceLoaderGDCM.h"
#include "VPL/Test/Utillities/referenceData.h"
#include <VPL/Test/Utillities/testingData.h>

namespace filters
{
//!
class ImageDFilter : public testing::Test
{
public:

	std::string dirPath;
	vpl::test::ReferenceData config;

	vpl::img::CDicomSlicePtr inSlice;
	vpl::img::CDicomSlicePtr slice;

	void SetUp() override
	{
        dirPath = vpl::test::TestingData::getDirectoryPath();
	}
	void TearDown() override
	{
	}

	//! \brief 
	//! \param image 
	//! \param marginSize 
	static void fixMargin(vpl::img::CDicomSlicePtr& image, int marginSize)
	{
		if (image->getMargin() < marginSize)
		{
			const vpl::tSize sx(image->getXSize());
			const vpl::tSize sy(image->getYSize());

			// Create data
			vpl::img::CDicomSlicePtr s(new vpl::img::CDicomSlice(sx, sy, marginSize));

			// Copy source
			for (vpl::tSize y = 0; y < sy; ++y)
				for (vpl::tSize x = 0; x < sx; ++x)
					s->set(x, y, image->at(x, y));
			// Create margin
			s->mirrorMargin();
			// Store slice
			image = s;
		}
	}

	void LoadSlice(vpl::img::CDicomSlice& slice, const std::string& fileName) const
	{
		const std::string path = dirPath + '/' + fileName;

		bool bOK = false;
#if defined( VPL_USE_GDCM )

		 vpl::img::CDicomSliceLoaderGDCM m_Loader;
		 bOK = m_Loader.loadDicom(path, slice, true);

#else
		vpl::img::CDicomSliceLoaderVPL m_Loader;
		bOK = m_Loader.loadDicom(path, slice, true);

#endif
		if (!bOK)
		{
			FAIL() << "Dicom: " << path << " is not loaded";
		}
	}
};


//! Tesing if comparation method is OK.
TEST_F(ImageDFilter, CheckTestingCompareMethod)
{

	LoadSlice(*inSlice, "images/asskull.dcm");
	LoadSlice(*slice, "images/asskull.dcm");
	double metrics;
	ASSERT_TRUE(vpl::test::metrics::ad(*inSlice, *slice, metrics));
	ASSERT_FLOAT_EQ(0, metrics) << "Image metrics ad failed";

	ASSERT_TRUE(vpl::test::metrics::psnr(*inSlice, *slice, metrics)) << "Failed comparing images";
	ASSERT_EQ(INFINITY, metrics) << "Image metrics psnr failed";

}


//! Gauss Filter on dicom slices
TEST_F(ImageDFilter, Gauss)
{
	ASSERT_TRUE(config.load(dirPath, "images/gaussD")) << "No config loaded";
	std::string commonInput;
	if (config.getGlobalInput(commonInput))
	{
		LoadSlice(*inSlice, "images/" + commonInput);
	}

	for (int testId = 0; testId < config.getTestCount(); testId++)
	{
		vpl::img::CDicomSlicePtr testSlice(new vpl::img::CDicomSlice(*inSlice));
		std::string input;
		if (config.getInput(input, testId))
		{
			const vpl::img::CDicomSlicePtr inputSlice;
			LoadSlice(*inputSlice, input);
			testSlice = inputSlice;
		}
		const std::string name = config.getString("name", testId);
		const std::string refImage = config.getString("reference", testId);
		const float sigma = config.getNumber<float>("sigma", testId);
		int psnrRequired = config.getNumber<int>("psnr", testId);

		vpl::img::CGaussFilter<vpl::img::CDicomSlice> filter(sigma);
		LoadSlice(*slice, "images/" + refImage);

		fixMargin(testSlice, filter.getSize() >> 1);

		const vpl::img::CDicomSlicePtr spFiltered(new vpl::img::CDicomSlice(testSlice->getXSize(), testSlice->getYSize()));
		filter(*testSlice, *spFiltered);

		double psnr;
		ASSERT_TRUE(vpl::test::metrics::psnr(*spFiltered, *slice, psnr)) << "Failed comparing images in test:" << name;
		EXPECT_GE(psnr, psnrRequired) << "Image metrics psnr failed in test:" << name;
	}
}
}
