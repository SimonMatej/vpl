//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#if defined _M_X64

#include <gtest/gtest.h>

#include <VPL/Math/StaticVector.h>
#include <VPL/Test/Compare/compare1D.h>
namespace staticVector
{
//! Test fixture
template<class T>
class StaticVectorTest : public testing::Test
{
public:
	typedef  T type;

	//! Object which compare required and input values.
	vpl::test::Compare1D<type, vpl::math::CStaticVector<type, 3> > compare;

	vpl::math::CStaticVector<type, 3> vector1;
	vpl::math::CStaticVector<type, 3> vector2;

	void SetUp() override
	{
		compare.setErrorMessage("Vectors differ at index");
		createSequence<3>(vector2);

	}

	//! Generate 0,1,2,3 to vector size
	template<int M>
	static void createSequence(vpl::math::CStaticVector<type, M>& vector)
	{
		for (vpl::tSize j = 0; j < vector.size(); j++)
		{
			vector(j) = j;
		}
	}
};

//! Define types for typed testing
typedef testing::Types<int, double, float> implementations;
TYPED_TEST_CASE(StaticVectorTest, implementations);


TYPED_TEST(StaticVectorTest, Initialize)
{
	using type = typename TestFixture::type;
	vpl::math::CStaticVector<type, 4> v4(0, 1, 2, 3);

	vpl::test::Compare1D<type, vpl::math::CStaticVector<type, 4> > compare;

	compare.sequence(v4,4,0,1);

	TestFixture::vector1.fill(0);
}
TYPED_TEST(StaticVectorTest, Fill)
{
	using type = typename TestFixture::type;
	TestFixture::vector1.fill(0);
	TestFixture::compare.values(static_cast<type>(0), TestFixture::vector1, 3);
	TestFixture::vector1.fill(256);
	TestFixture::compare.values(256, TestFixture::vector1, 3);

}

TYPED_TEST(StaticVectorTest, Add)
{
	TestFixture::vector1.fill(0);

	TestFixture::vector1 += TestFixture::vector2;
	TestFixture::compare.sequence(TestFixture::vector1, 3, 0, 1);

	TestFixture::vector1 += vpl::CScalar<int>(10);

	TestFixture::compare.sequence(TestFixture::vector1, 3, 10, 1);

}

TYPED_TEST(StaticVectorTest, Multiply)
{
	TestFixture::vector2 *= vpl::CScalar<int>(2);
	TestFixture::compare.sequence(TestFixture::vector2, 3, 0, 2);
}
TYPED_TEST(StaticVectorTest, Zeros)
{
	using type = typename TestFixture::type;
	TestFixture::vector1.zeros();	
	TestFixture::compare.values(static_cast<type>(0), TestFixture::vector1, 3);
}

TYPED_TEST(StaticVectorTest, Ones)
{
	using type = typename TestFixture::type;
	TestFixture::vector1.ones();
	TestFixture::compare.values(1, TestFixture::vector1, 3);
}


} // namespace staticVector

#endif