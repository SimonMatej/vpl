//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/


#include <gtest/gtest.h>
#include <VPL/Image/Size.h>
#include <VPL/Math/Vector.h>
#include <VPL/Test//Compare/compare1D.h>
#include <VPL/Math/Algorithm/FuzzyCMeans.h>
#include <VPL/Test/Utillities/multiSetData.h>



namespace fuzzyCMeans
{
namespace settings
{
const vpl::tSize numOfSamples = 1000;

}
//! Test fixtures
class FuzzyCMeansTest : public testing::Test
{
public:
    vpl::tSize THIRD{};
    vpl::math::CFVector v1;
    vpl::test::Compare1D<float, vpl::math::CFVector> compareFloat;
    vpl::test::Compare1D<double, vpl::math::CVector<double>> compareDouble;
    vpl::test::MultiSetData<float> requiredData;
    vpl::math::CFuzzyCMeans<vpl::math::CFVector, 1> Clustering;

    void SetUp() override
    {
        THIRD = settings::numOfSamples / 3;
        v1 = vpl::math::CFVector(settings::numOfSamples);
        v1.fill(1);
        v1.asEigen().segment(THIRD, THIRD).fill(2);
        v1.asEigen().segment(2 * THIRD, THIRD).fill(3);
    }

    void testMemberShip(const int i)
    {
        vpl::math::CFuzzyCMeans<vpl::math::CFVector, 1>::tVector v;
        Clustering.getMembership(i, v);
        for (vpl::tSize index = 0; index < v.getSize(); ++index)
        {
            float d = v.at(index);
            requiredData.checkValue(v.at(index));
        }
        requiredData.reset();
    }

};

//! Checks if value for clustering are:
//! 111...222...333...
TEST_F(FuzzyCMeansTest, Initialization)
{
    compareFloat.range(1, v1, 0, THIRD);
    compareFloat.range(2, v1, THIRD, THIRD);
    compareFloat.range(3, v1, 2 * THIRD, THIRD);
}

//! Test clustering method and outputs.
TEST_F(FuzzyCMeansTest, Clustering)
{
    ASSERT_TRUE(Clustering.execute(v1));

    vpl::math::CFuzzyCMeans<vpl::math::CFVector, 1>::tCluster c;
    ASSERT_EQ(3, Clustering.getNumOfClusters());

    requiredData.require(1.0);
    requiredData.require(2.0);
    requiredData.require(3.0);

    for (vpl::tSize i = 0; i < Clustering.getNumOfClusters(); ++i)
    {
        ASSERT_TRUE(Clustering.getCluster(i, c));
        requiredData.checkValue(c.at(0));
    }
	requiredData.checkUsedAll();
    requiredData.reset();
    vpl::math::CFuzzyCMeans<vpl::math::CFVector, 1>::tVector v;

    Clustering.getMembership(150, v);
    double test1[] = { 0,0,1 };
    compareDouble.values(test1, v, v.size());

    Clustering.getMembership(450, v);
    double test2[] = { 0,1,0 };
    compareDouble.values(test2, v, v.size());

    Clustering.getMembership(750, v);
    double test3[] = { 1,0,0 };
    compareDouble.values(test3, v, v.size());
}
}