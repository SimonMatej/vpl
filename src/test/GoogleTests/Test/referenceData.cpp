//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/


#include "gtest/gtest.h"
#include <VPL/Test/Utillities/arguments.h>
#include "VPL/Test/Utillities/referenceData.h"
namespace filters
{

//! Testing filters on CImage16 
class ReferenceData : public testing::Test
{
public:

	std::string dirPath;

	vpl::test::ReferenceData config;
	bool isConfigLoaded{};

	void SetUp() override
	{
		vpl::test::Arguments arguments = vpl::test::Arguments::get();
		if (!arguments.value("dir", this->dirPath))
		{
			//! Use directory where was test runned.
			dirPath = "./data";
		}
	}

};

//! Tesing if comparation method is OK.
TEST_F(ReferenceData,Test)
{
	//! Check if is loaded config file
	ASSERT_TRUE(config.load(dirPath, "referenceData")) << "No config loaded";

	//! Iterate over all test from config file
	for (int id = 0; id < config.getTestCount(); id++)
	{
		int index, width;
		const bool hasIndex = config.getBool("has_index", id);
		const bool hasWidth = config.getBool("has_width", id);

		if (config.getBool("has_index", id))
		{
			index = config.getNumber<int>("index", id);
		}
		if (config.getBool("has_width", id))
		{
			width = config.getNumber<int>("width", id);
		}
		switch(id)
		{
			case 0:
				ASSERT_FALSE(hasIndex);
				ASSERT_FALSE(hasWidth);
				break;
			case 1:
				ASSERT_TRUE(hasIndex);
				ASSERT_FALSE(hasWidth);
				ASSERT_EQ(1, index);
				break;
			case 2:
				ASSERT_TRUE(hasIndex);
				ASSERT_TRUE(hasWidth);
				ASSERT_EQ(2, index);
				ASSERT_EQ(25, width);
				break;
		default: 
			FAIL() << "Test is now supported only for 3 tests.";
		}
	}
}
}